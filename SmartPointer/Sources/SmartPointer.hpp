#ifndef SMARTPOINTER_SMART_POINTER_HPP
#define SMARTPOINTER_SMART_POINTER_HPP

template <typename T> class SmartPointer {
public:
  SmartPointer();
  explicit SmartPointer(T *newPtr);

  ~SmartPointer();

  SmartPointer &operator=(T *newPtr);

  T &operator*() const;
  T *operator->() const;

  bool operator==(const SmartPointer &sPtr) const;

  T *Release();

private:
  void Set(T *newPtr);

  T *ptr_;
};

template <typename T> SmartPointer<T>::SmartPointer() : ptr_(nullptr) {}

template <typename T> SmartPointer<T>::SmartPointer(T *newPtr) : ptr_(newPtr) {}

template <typename T> SmartPointer<T>::~SmartPointer() { delete ptr_; }

template <typename T> T *SmartPointer<T>::Release() {
  T *ptr = ptr_;
  ptr_ = nullptr;

  return ptr;
}

template <typename T> SmartPointer<T> &SmartPointer<T>::operator=(T *newPtr) {
  Set(newPtr);
  return *this;
}

template <typename T> T &SmartPointer<T>::operator*() const { return *ptr_; }

template <typename T> T *SmartPointer<T>::operator->() const { return ptr_; }

template <typename T> bool SmartPointer<T>::operator==(const SmartPointer<T> &sPtr) const { return sPtr.ptr_ == ptr_; }

template <typename T> void SmartPointer<T>::Set(T *newPtr) {
  if (newPtr != ptr_) {
    delete ptr_;
    ptr_ = newPtr;
  }
}

#endif // SMARTPOINTER_SMART_POINTER_HPP
