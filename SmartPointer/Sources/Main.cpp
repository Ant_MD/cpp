#include "SmartPointer.hpp"

#include <iostream>

using namespace std;

class Foo {
public:
  explicit Foo(int id) : id_(id) { cout << "Create  Foo " << id_ << endl; };
  ~Foo() { cout << "Destroy Foo " << id_ << endl; };

  int id_;
};

int main() {
  SmartPointer<Foo> a(new Foo(1));

  SmartPointer<Foo> b;
  b = new Foo(2);
  b = nullptr;

  auto *f = new Foo(3);
  SmartPointer<Foo> c(f);
  f = c.Release();
  delete f;

  return 0;
}
