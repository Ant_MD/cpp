#include "IniConfig.hpp"

#include <algorithm>

using namespace std;

IniConfig::IniConfig() : properties_() {}

IniConfig::~IniConfig() = default;

void IniConfig::Load(istream &stream) {
  string currSection;

  string line;

  while (getline(stream, line)) {
    line = Trim(line);

    if (!line.empty()) {
      if (line.front() == ';') {
      } else if (line.front() == '[' && line.back() == ']' && line.size() >= 3) {
        currSection = Trim(line.substr(1, line.size() - 2) + '.');

        transform(currSection.begin(), currSection.end(), currSection.begin(), ::tolower);
      } else {
        size_t pos = line.find('=');

        if (pos != string::npos) {
          string key = line.substr(0, pos);
          string value = Trim(line.substr(pos + 1, line.length()));

          if (!value.empty()) {
            transform(key.begin(), key.end(), key.begin(), ::tolower);

            properties_[currSection + key] = value;
          }
        }
      }
    }
  }
}

void IniConfig::Clear() { properties_.clear(); }

std::string IniConfig::GetString(const string &key) { return properties_[key]; }

void IniConfig::AddString(const std::string &key, const std::string &value) { properties_[key] = value; }

std::string IniConfig::Trim(const std::string &string) {
  if (!string.empty()) {
    size_t begin = string.find_first_not_of(' ');
    size_t end = string.find_last_not_of(' ');

    if (begin != string::npos && end != string::npos) {
      return string.substr(begin, end + 1);
    } else {
      return "";
    }

  } else {
    return string;
  }
}
