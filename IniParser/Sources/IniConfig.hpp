#ifndef INIPARSER_INI_CONFIG_HPP
#define INIPARSER_INI_CONFIG_HPP

#include <istream>
#include <string>
#include <unordered_map>

class IniConfig {
public:
  IniConfig();
  ~IniConfig();

  void Load(std::istream &stream);
  void Clear();

  std::string GetString(const std::string &key);
  void AddString(const std::string &key, const std::string &value);

private:
  static std::string Trim(const std::string &string);

  std::unordered_map<std::string, std::string> properties_;
};

#endif // INIPARSER_INI_CONFIG_HPP
