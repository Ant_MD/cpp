#include "IniConfig.hpp"

#include <fstream>
#include <iostream>

using namespace std;

void PrintProperty(IniConfig config, const string &key) { cout << key << "=" << config.GetString(key) << endl; }

int main() {
  ifstream input1("Example1.ini");
  ifstream input2("Example2.ini");
  ifstream input3("Example3.ini");

  if (input1.is_open()) {
    cout << "Example1.ini:" << endl;
    IniConfig config;
    config.Load(input1);

    PrintProperty(config, "key1");
    PrintProperty(config, "section1.key2");
    PrintProperty(config, "section1.key3");
    PrintProperty(config, "section2.key4");
    PrintProperty(config, "section2.key5");
    PrintProperty(config, "section2.key6");
    PrintProperty(config, "section2.key7");
  }

  if (input2.is_open()) {
    cout << "Example2.ini:" << endl;

    IniConfig config;
    config.Load(input2);

    PrintProperty(config, "key1");
    PrintProperty(config, "section1.key2");
    PrintProperty(config, "section1.key3");
    PrintProperty(config, "section2.key4");
    PrintProperty(config, "section2.key5");
    PrintProperty(config, "section2.key6");
    PrintProperty(config, "section2.key7");
  }

  if (input3.is_open()) {
    cout << "Example3.ini:" << endl;

    IniConfig config;
    config.Load(input3);

    PrintProperty(config, "key");
  }

  return 0;
}
