#ifndef BATTLESHIP_PLAYER_HPP
#define BATTLESHIP_PLAYER_HPP

#include "Vec2.hpp"

enum class TurnResult { MISS, KILL, WOUND, LOSE };

class Player {
public:
  virtual ~Player() = default;

  virtual Vec2 NextTurn() = 0;
  virtual void UpdateEnemyField(const Vec2 &tile, TurnResult result) = 0;

  virtual TurnResult UpdateOwnField(const Vec2 &tile) = 0;
};

#endif // BATTLESHIP_PLAYER_HPP
