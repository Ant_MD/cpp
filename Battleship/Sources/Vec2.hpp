#ifndef BATTLESHIP_VEC2_HPP
#define BATTLESHIP_VEC2_HPP

class Vec2 {
public:
  Vec2();
  Vec2(int x, int y);

  bool operator==(const Vec2& other);

  int x_;
  int y_;
};

#endif // BATTLESHIP_VEC2_HPP
