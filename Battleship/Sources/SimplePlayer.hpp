#ifndef BATTLESHIP_SIMPLE_PLAYER_HPP
#define BATTLESHIP_SIMPLE_PLAYER_HPP

#include "Player.hpp"

#include <vector>

enum class TileState { UNKNOWN, WOUNDED_SHIP, EMPTY };

class SimplePlayer : public Player {
public:
  SimplePlayer();
  ~SimplePlayer() = default;

  Vec2 NextTurn() override;
  void UpdateEnemyField(const Vec2 &tile, TurnResult result) override;

  TurnResult UpdateOwnField(const Vec2 &tile) override;

private:
  std::vector<std::vector<Vec2>> ships_;
  std::vector<TileState> enemyField_;

  TileState field[10][10];
};

#endif // BATTLESHIP_SIMPLE_PLAYER_HPP