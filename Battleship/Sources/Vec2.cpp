#include "Vec2.hpp"

Vec2::Vec2() : x_(0), y_(0) {}

Vec2::Vec2(int x, int y) : x_(x), y_(y) {}

bool Vec2::operator==(const Vec2 &other) { return x_ == other.x_ && y_ == other.y_; }
