#include "SimplePlayer.hpp"

#include <iostream>
#include <memory>

using namespace std;

Vec2 parseVec2(const std::string &input) {
  Vec2 result;
  result.x_ = input[0] - 'A';

  string *secondCoord = new string(input, 1);

  result.y_ = std::stoi(*secondCoord);

  result.y_--;
  return result;
}

string showVec2(const Vec2 &vec) {
  std::string str;
  str.append(1, (char)(vec.x_ + 'A'));
  int y = vec.y_;
  y++;
  str += to_string(y);
  return str;
}

int main(int argc, char *argv[]) {
  shared_ptr<Player> player(new SimplePlayer());

  bool ownTurn;

  if (argc <= 1) {
    ownTurn = false;
  } else {
    ownTurn = argv[1][0] == '0';
  }

  while (true) {
    if (ownTurn) {
      Vec2 target = player->NextTurn();
      cout << target << "\n";

      string input;
      getline(cin, input);

      if (input == "miss") {
        player->UpdateEnemyField(target, TurnResult::MISS);
      } else if (input == "kill") {
        player->UpdateEnemyField(target, TurnResult::KILL);
        continue;
      } else if (input == "wound") {
        player->UpdateEnemyField(target, TurnResult::WOUND);
        continue;
      } else {
        player->UpdateEnemyField(target, TurnResult::LOSE);
        break;
      }

    } else {
      string input;
      getline(cin, input);

      Vec2 targetedCell = parseVec2(input);

      TurnResult result = player->UpdateOwnField(targetedCell);

      if (result == TurnResult::MISS) {
        cout << "miss" << endl;
      } else if (result == TurnResult::KILL) {
        cout << "kill" << endl;
        continue;
      } else if (result == TurnResult::WOUND) {
        cout << "wound" << endl;
        continue;
      } else {
        cout << "lose" << endl;
        break;
      }
    }

    ownTurn = !ownTurn;
  }
  return 0;
}
