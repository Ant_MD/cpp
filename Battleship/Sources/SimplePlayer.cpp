#include "SimplePlayer.hpp"

#include <algorithm>
#include <chrono>
#include <random>

using namespace std;

int RandInt(int min, int max) {
  static default_random_engine gen(chrono::system_clock::now().time_since_epoch().count());

  uniform_int_distribution<int> dist(min, max);

  return dist(gen);
}

vector<Vec2> TryPlace(int length, vector<bool> &field) {
  bool vertical = RandInt(0, 1);

  Vec2 start;

  do {
    if (vertical) {
      start = Vec2(RandInt(0, 9), RandInt(0, 9 - length));
    } else {
      start = Vec2(RandInt(0, 9 - length), RandInt(0, 9));
    }
  } while (field[start.x_ + start.y_ * 10]);

  vector<Vec2> ship;

  for (int i = 0; i < length; i++) {
    if (vertical) {
      ship.emplace_back(start.x_, start.y_ + i);
    } else {
      ship.emplace_back(start.x_ + i, start.y_);
    }
  }

  for (const auto& part : ship) {
    if (field[part.x_ + part.y_ * 10]) {
      return vector<Vec2>();
    }
  }

  int x = max(0, start.x_ - 1);
  int y = max(0, start.y_ - 1);

  int endX;
  int endY;

  if (vertical) {
    endX = min(9, start.x_ + 1);
    endY = min(9, start.y_ + length);
  } else {
    endX = min(9, start.x_ + length);
    endY = min(9, start.y_ + 1);
  }

  for (int i = x; i <= endX; i++) {
    for (int j = y; j <= endY; j++) {
      field[i + j * 10] = true;
    }
  }

  return ship;
}

vector<vector<Vec2>> PlaceAll() {
  vector<vector<Vec2>> ships;

  vector<bool> field(100, false);

  for (int i = 1; i <= 4; i++) {
    for (int j = 0; j < (5 - i); j++) {
      vector<Vec2> ship;

      while (ship.empty()) {
        ship = TryPlace(i, field);
      }

      ships.push_back(ship);
    }
  }

  return ships;
}

SimplePlayer::SimplePlayer() : enemyField_(100, TileState::UNKNOWN) { ships_ = PlaceAll(); }

Vec2 SimplePlayer::NextTurn() {
  Vec2 next;

  do {
    next = Vec2(RandInt(0, 9), RandInt(0, 9));
  } while (enemyField_[next.x_ + next.y_ * 10] != TileState::UNKNOWN);

  return next;
}

void SimplePlayer::UpdateEnemyField(const Vec2 &tile, TurnResult result) {
  if (result == TurnResult::WOUND || result == TurnResult::KILL) {
    enemyField_[tile.x_ + tile.y_ * 10] = TileState::WOUNDED_SHIP;
  } else if (result == TurnResult::MISS) {
    enemyField_[tile.x_ + tile.y_ * 10] = TileState::EMPTY;
  }
}

TurnResult SimplePlayer::UpdateOwnField(const Vec2 &tile) {
  for (auto &ship : ships_) {
    auto it = find(ship.begin(), ship.end(), tile);

    if (!ship.empty() && it != ship.end()) {
      ship.erase(it);

      if (ship.empty()) {
        for (auto ship : ships_) {
          if (!ship.empty()) {

            return TurnResult::KILL;
          }
        }

        return TurnResult::LOSE;
      } else {
        return TurnResult::WOUND;
      }
    }
  }
  return TurnResult::MISS;
}
